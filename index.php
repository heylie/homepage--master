<html lang="zh-CN">

<head>

    <meta charset="UTF-8">
    <title>顾渊渡のHome|这是一个引导页面哦！</title>
    <!--为搜索引擎定义关键词-->
    <meta name="keywords" content="顾渊渡,个人主页,homepage,博客,引导页">
    <!--为网页定义描述内容 用于告诉搜索引擎，你网站的主要内容-->
  <!--  <meta name="description" content="顾渊渡的个人主页">--->
    <!--定义网页作者-->
    <meta name="author" content="顾渊渡">
    <!--网站版权-->
    <meta name="copyright" content="顾渊渡">
    <!--指定IE和Chrome使用最新版本渲染当前页面-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--指导浏览器如何缓存某个响应以及缓存多长时间-->
    <!--no-cache:先发送请求,与服务器确认该资源是否被更改,如果未被更改,则使用缓存
    no-store:不允许缓存,每次都要去服务器上,下载完整的响应(安全措施)
    public:缓存所有响应,但并非必须,因为max-age也可以做到相同效果
    private:只为单个用户缓存,因此不允许任何中继进行缓存,(比如说CDN就不允许缓存private的响应)
    maxage:当前请求开始,该响应在多久内能被缓存和重用,而不去服务器重新请求,例:max-age=60表示响应可以再缓存和重用60秒
    -->
    <meta http-equiv="cache-control" content="no-cache">
    <!--禁止百度自动转码 用于禁止当前页面在移动端浏览时,被百度自动转码,虽然百度的本意是好的,但是转码效果很多时候却不尽人意-->
    <!--meta http-equiv="Cache-Control" content="no-siteapp" /-->
    <!-- 分享网页时显示的标题-QQ-->
    <meta itemprop="name" content="顾渊渡 | 离开池鱼的往昔！" />
    <!-- 分享网页时显示的缩略图-QQ-->
    <meta itemprop="image" content="./blog.ico" />
    <!--分享网页时时显示的内容-QQ-->
    <meta name="description" itemprop="description" content="顾渊渡 | 在这里，我们共同探索未知领域，分享见解与感悟，助你成长之路一帆风顺" />
    <!--设置自动适应电脑和手机屏幕-->
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0, user-scalable=no minimal-ui">
    <!--设置浏览器栏favicon图标-->
    <link rel="icon" href="./blog.ico" type="image/x-icon" />
    <!--定义搜索引擎爬虫的索引方式-->
    <!--index,follow:可以抓取本页，而且可以顺着本页继续索引别的链接
    noindex,follow:不许抓取本页，但是可以顺着本页抓取索引别的链接
    index,nofollow:可以抓取本页，但是不许顺着本页抓取索引别的链接
    noindex,nofollow:不许抓取本页，也不许顺着本页抓取索引别的链接
    -->
    <meta name="robots" content="index,follow">
    <!--引入css文件-->
    <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/gradient-text.css"><!--底部-->
    <link rel="stylesheet" href="//at.alicdn.com/t/font_1092713_tcnnod74va9.css">
    <meta name="baidu-site-verification" content="codeva-Ow2vnT6soH" />
    <script src="https://www.heylie.cn/js/min.js"></script>
    <script src="https://www.heylie.cn/js/mes.js"></script>
    <!--底部-->

</head>

<body>

    <!--网页主体框架-->
    <div class="centent">

        <div class="mian">
            <!-- 上部分 -->
            <div class="main-top">
                <div class="main-top-title">
                    <!-- 大标题 -->
                    <h1>Hello!</h1>
                    <!-- 头像 -->
                    <a href=""><img src="./blog.ico" alt="咦，图片消失了"></a>
                </div>
                <!-- 简介 -->
                <div class="main-top-Brief">
                     <p><script src="https://api.qoc.cc/api/yan"></script></p>
                  
                </div>
            </div>
            <!-- 下部分 -->
            <div class="main-bot">
                <!-- 小图标区域 -->
                <div class="main-bot-tb">
                    <a href="https://blog.heylie.cn" target="_blank" class="main-bot-tb-1" title="博客"><i class="iconfont icon-cnblogs-grey"></i></a>
                    
                    
                    <a href="https://space.bilibili.com/611045521" target="_blank" class="main-bot-tb-2" title="B站"><i class="iconfont icon-bilibili-fill1"></i></a>
                    
                    
                    <a href="https://qm.qq.com/q/cS6LXgXrOM" target="_blank" class="main-bot-tb-3" title="QQ"><i class="iconfont icon-QQ"></i></a>
                    
                    
                    <a href="mailto:admin@dwo.cc" data-bs-toggle="tooltip" data-bs-placement="top" title="" target="_blank" class="main-bot-tb-4" data-bs-original-title="博主邮箱"><i class="iconfont icon-xinxi"></i></a>
                </div>
                
                <!-- 下部分副标题 -->
                <div class="main-bot-title">
                   <h3>我的站点</h3>
                </div>
                
                <!-- 下部分内容区 -->
                <div class="main-bot-nr">
                    <a href="https://blog.heylie.cn" target="_blank" class="main-bot-nr-kp">
                        <div class="main-bot-nr-kp-k">
                            <div class="main-bot-nr-kp-k-img"><img src="./assets/img/1.webp" alt="顾渊渡" class="顾渊渡"></div>
                           <div class="main-bot-nr-kp-k-n">
                               <span class="p1">顾渊渡</span>
                               <span class="p2">我的个人网站啦</span>
                           </div>
                        </div>
                    </a>
                    <a href="http://www.dwo.cc" target="_blank" class="main-bot-nr-kp">
                        <div class="main-bot-nr-kp-k">
                            <div class="main-bot-nr-kp-k-img"><img src="./assets/img/2.webp" alt="顾渊渡" class="顾渊渡的个人博客"></div>
                           <div class="main-bot-nr-kp-k-n">
                               <span class="p1">顾渊渡的域名二</span>
                               <span class="p2">用于记录自己的前端学习、生活等</span>
                           </div>
                        </div>
                    </a>
                    <a href="https://dh.heylie.cn/" target="_blank" class="main-bot-nr-kp">
                        <div class="main-bot-nr-kp-k">
                            <div class="main-bot-nr-kp-k-img"><img src="./assets/img/3.webp" alt="顾渊渡" class="顾渊导航"></div>
                           <div class="main-bot-nr-kp-k-n">
                               <span class="p1">顾渊导航</span>
                               <span class="p2">一个小小导航！</span>
                           </div>
                        </div>
                    </a>
                    <a href="https://api.heylie.cn/" target="_blank" class="main-bot-nr-kp">
                        <div class="main-bot-nr-kp-k">
                            <div class="main-bot-nr-kp-k-img"><img src="./assets/img/4.webp" alt="顾渊渡" class="小小数据-API"></div>
                           <div class="main-bot-nr-kp-k-n">
                               <span class="p1">小小数据-API</span>
                               <span class="p2">稳定、快速、免费的 API 接口服务</span>
                           </div>
                        </div>
                    </a>
            
                    
                </div>
                <!-- 下部分内容区 end-->
                
            </div>
        </div>

        <!-- 版权声明 -->
        <div class="footer">
            Copyright © 2021 - <span>2024</span> 
            <a href="https://www.heylie.cn" class="footer-link">顾渊渡</a> All Rights Reserved.<br>
           <p>
                <a href="https://beian.miit.gov.cn/" class="gradient-text icp" target="_blank">
                    <img src="https://cdn.heylie.cn/tb/icpbeian.png" style="width: 20px;" alt="赣ICP备2023002241号">
                    赣ICP备2023002241号
                </a>
                <span class="separator">&nbsp;|&nbsp;</span>
                <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=36012102000605" class="gradient-text gonganbei" target="_blank">
                    <img src="https://cdn.heylie.cn/tb/gongwanganbei.png" style="width: 18px;" alt="赣公网安备 36012102000605号">
                    赣公网安备 36012102000605号
                </a>
                <span class="separator">&nbsp;|&nbsp;</span>
                <a href="https://dnspod.cloud.tencent.com/" class="gradient-text tencentcloud" target="_blank">
                    <img src="https://cloud.tencent.com//favicon.ico" style="width: 20px;" alt="腾讯云">
                    由腾讯云提供域名支持
                </a>
                <span class="separator">&nbsp;|&nbsp;</span>
                <a href="https://www.aliyun.com/" class="gradient-text aliyunecs" target="_blank">
                    <img src="https://cdn.heylie.cn/tb/ali.png" style="width: 50px;" alt="阿里云ECS">
                    由阿里云ECS服务器支持
                </a>
                <span class="separator">&nbsp;|&nbsp;</span>
                <a href="https://www.dogecloud.com/" class="gradient-text duojiyun" target="_blank">
                    <img src="https://cdn.heylie.cn/tb/duojiyun.png" style="width: 20px;" alt="多吉云">
                    由多吉云提供CDN支持
                </a>
            </p>
    
    </div>
</div>

<script>
//跳转警告
document.addEventListener('DOMContentLoaded', function() {
    var externalLinks = document.querySelectorAll('a[href^="http"]:not([href^="' + location.origin + '"])');
    for (var i = 0; i < externalLinks.length; i++) {
        var link = externalLinks[i];
        link.href = 'https://blog.heylie.cn/go.php?url=' + encodeURIComponent(link.href);
    }
});
</script>
</body>

</html>